// Mad Lib
// Ken Vicchiollo

#include <iostream>
#include <fstream>
#include <conio.h>
#include <string>

using namespace std;

void PrintMadlib(string userResponses[12], ostream &os = cout)
{
	os << "One day my " << userResponses[0] << " friend and I decided to go to the " << userResponses[1];
	os << " game in " << userResponses[2] << "." << "\n";
	os << "We really wanted to see " << userResponses[3] << " play." << "\n";
	os << "So, we " << userResponses[4] << " in the " << userResponses[5] << " and headed down to the " << userResponses[6];
	os << " and bought some " << userResponses[7] << "." << "\n";
	os << "We watched the game and it was " << userResponses[8] << "." << "\n";
	os << "We ate some " << userResponses[9] << " and drank some " << userResponses[10] << "." << "\n";
	os << "We had a " << userResponses[11] << " time, and can't wait to go again." << "\n";
}

int main()
{
	string wordsToInput[12] = {
		"an adjective (describing word)",
		"a sport",
		"a city",
		"a person",
		"an action verb (past tense)",
		"a vehicle",
		"a place",
		"a noun (thing, plural)",
		"an adjective (describing word)",
		"a food (plural)",
		"a liquid",
		"an adjective (describing word)"
	};

	string userResponses[12];

	for (int i = 0; i < 12; i++)
	{
		cout << "Enter " << wordsToInput[i] << ": ";
		getline(cin, userResponses[i]);
	}
	 
	cout << "\n" << "\n";
	PrintMadlib(userResponses);
	cout << "\n" << "\n";

	char saveFile;
	cout << "Would you like to save the output to a file? (y/n): ";
	cin >> saveFile;

	if (saveFile == 'y' || saveFile == 'Y') 
	{
		string filePath = "C:\\Users\\Public\\test.txt";
		ofstream ofs(filePath);
		PrintMadlib(userResponses, ofs);

		cout << "\n" << "Mad Lib was saved to " << filePath;
	}
	else
	{
		cout << "\n" << "Mad Lib was not saved.";
	}
	
	cout << "\n" << "\n" << "Goodbye!!" << "\n" << "\n";

	(void)_getch();
	return 0;
}
